# Welcome to Advanced Microprocessor Design

This is the course website of Advanced Microprocessor Design, instructed Dr. Zhangxi Tan.

## Semester/Year

Fall 2022

## Class Hours

7:20 - 9:45 p.m. (Wed)

## Classroom

C3 2005, Nanshan Intelligence IndustrialPark, Xili, Nanshan, Shenzhen

## Catalogue Listing

Concepts and design of different components of advanced microprocessors.

Topics: out-of-order CPU design, performance modeling, exception handling, cache systems, instruction scheduling, branch prediction, etc.

Open source EDA tools, open source RISC-V designs and ecosystem.

## Textbook
<div style="text-align:justify;text-justify:inter-ideograph">
Computer Architecture, Sixth Edition: A Quantitative Approach
</div>
