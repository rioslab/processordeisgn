# Syllabus

## Grading Policy

<div style="text-align:justify;text-justify:inter-ideograph">
  <table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1" >
    <thead>
      <tr class="header">
        <th style="text-align: center;">Evaluation Type</th>
        <th style="text-align: center;">Overall Percent</th>
      </tr>
    </thead>
    <tbody>
      <tr class="odd">
          <td style="text-align: center;">Paper Reading &#38 Discussion</td>
          <td style="text-align: center;">20%</td>
      </tr>
    <tr class="even">
    <td style="text-align: center;">Lab 1</td>
    <td style="text-align: center;">10%</td>
    </tr>
    <tr class="odd">
    <td style="text-align: center;">Lab 2</td>
    <td style="text-align: center;">10%</td>
    </tr>
    <tr class="even">
    <td style="text-align: center;">Lab 3</td>
    <td style="text-align: center;">10%</td>
    </tr>
    <tr class="odd">
    <td style="text-align: center;">Final Project</td>
    <td style="text-align: center;">50%</td>
    </tr>
    </tbody>
    </table>

    <p>The project evaluation is based on PPA. </p>
</div>

##  Pre List
<div style="text-align:justify;text-justify:inter-ideograph">
  <table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1" >
    <thead>
      <tr class="header">
        <th style="text-align: center;">CPU Reading</th>
        <th style="text-align: center;">1</th>
        <th style="text-align: center;">2</th>
        <th style="text-align: center;">3</th>
        <th style="text-align: center;">4</th>
        <th style="text-align: center;">5</th>
        <th style="text-align: center;">6</th>
        <th style="text-align: center;">7</th>
        <th style="text-align: center;">8</th>
        <th style="text-align: center;">9</th>
        <th style="text-align: center;">10</th>
      </tr>
    </thead>
    <tbody>
      <tr class="odd">
          <th style="text-align: center;">Name</th>
          <td style="text-align: center;">郭沛晨</td>
          <td style="text-align: center;">胡深威</td>
          <td style="text-align: center;">王铭梓</td>
          <td style="text-align: center;">栾政轩</td>
          <td style="text-align: center;">张东宇</td>
          <td style="text-align: center;">冯大纬</td>
          <td style="text-align: center;">邓逸涛</td>
          <td style="text-align: center;">陈铭峰</td>
          <td style="text-align: center;">许昊</td>
          <td style="text-align: center;">姚为</td>
      </tr>
    </tbody>
    </table>

  <table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1" >
    <thead>
      <tr class="header">
        <th style="text-align: center;">System Reading</th>
        <th style="text-align: center;">1</th>
        <th style="text-align: center;">2</th>
        <th style="text-align: center;">3</th>
        <th style="text-align: center;">4</th>
        <th style="text-align: center;">5</th>
        <th style="text-align: center;">6</th>
        <th style="text-align: center;">7</th>
        <th style="text-align: center;">8</th>
        <th style="text-align: center;">9</th>
        <th style="text-align: center;">10</th>
        <th style="text-align: center;">11</th>
      </tr>
    </thead>
    <tbody>
      <tr class="odd">
      `   <th style="text-align: center;">Name</th>
          <td style="text-align: center;">郭沛晨</td>
          <td style="text-align: center;">张乙海</td>
          <td style="text-align: center;">胡深威</td>
          <td style="text-align: center;">王铭梓</td>
          <td style="text-align: center;">栾政轩</td>
          <td style="text-align: center;">张东宇</td>
          <td style="text-align: center;">冯大纬</td>
          <td style="text-align: center;">邓逸涛</td>
          <td style="text-align: center;">陈铭峰</td>
          <td style="text-align: center;">许昊</td>
          <td style="text-align: center;">姚为</td>
      </tr>
    </tbody>
    </table>

    <p>You can change the order. Let me know. </p>
</div>

## Course Schedule

<div style="text-align:justify;text-justify:inter-ideograph">
<table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1">
      <thead>
  <tr>
  <th style="text-align:center">Lecture</th>
  <th style="text-align:center">Date</th>
  <th style="text-align:center">Topic</th>
  <th style="text-align:center">Paper Reading</th>
  <th style="text-align:center">Textbook Reading</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <td style="text-align:center">1</td>
  <td style="text-align:center">09/12/2022<br>09/18/2022</td>
  <td style="text-align:center">Introduction and Early Machines</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://people.eecs.berkeley.edu/~kubitron/courses/cs252-S10/handouts/papers/b5000.pdf"> "Design of the B5000 System", Lonergan, King, 1961</a></p>
  <p><a target="_blank" href="https://www.ece.ucdavis.edu/~vojin/CLASSES/EEC272/S2005/Papers/IBM360-Amdahl_april64.pdf"> "Architecture of the IBM System/360", Amdahl, Blaauw, Brooks, 1964</a></p>
  </td>
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">2</td>
  <td style="text-align:center">09/21/2022</td>
  <td style="text-align:center">ISA,IBM360</td>
  <td style="text-align:center"></td>
  <td style="text-align:center"></td>

  </tr>
  <tr>
  <td style="text-align:center">3</td>
  <td style="text-align:center">09/28/2022</td>
  <td style="text-align:center">ROM/RAM, Microcode, RISC-V</td>

  <td style="text-align:center">
  <p><a target="_blank" href="https://ieeexplore.ieee.org/iel5/2/21180/00982918.pdf?casa_token=JoDwTTDDhnkAAAAA:dcxvHxhHyp7HFB5aIQEuqeHkhYDxmx6Fl8vLLV-dOGvLrtJVNzwHyPACcRcYhI0AITVE48JnSg"> "Asim: A Performance Model Framework", Joel Emer, 2002</a></p>
  <p><a target="_blank" href="https://dl.acm.org/doi/pdf/10.1145/641914.641917?casa_token=KfXrsQgT4tYAAAAA:XvcTRW9t3piyaajMuGYnzCTn13ew9JFgRJEl0_mzSD-I4yCGgbXbJwYDw-vF6TGGkSwRVJnTz_0R"> "The Case for the Reduced Instruction Set Computer", Patterson, Ditzel, 1980</a></p>
  <p><a target="_blank" href="https://dl.acm.org/doi/pdf/10.1145/641914.641918?casa_token=vkhkerYA_NkAAAAA:S8TcCAi92MvT6zDA_wfLd3Y_L0d1sDXhlVJg90_so-ZwjOP2Gm0b4CdfJ2mxo7MNMhuBjBCKk7e2"> Comments on the "The Case for the RISC", Clark, Strecker, 1980</a></p>
  <p><a target="_blank" href="https://inst.eecs.berkeley.edu//~cs252/sp17/papers/RISC-vs-CISC.pdf"> "Performance from architecture: comparing a RISC and CISC with similar hardware organization", Bhandarkar, Clark, 1991</a></p>
  ddl: 2022.10.05 19:00!
  </td>
  
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">4</td>
  <td style="text-align:center">10/12/2022</td>
  <td style="text-align:center">Pipeline, Hazard, Trap and Interrupt: 1</td>
  <td style="text-align:center"></td>
    <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">5</td>
  <td style="text-align:center">10/19/2022</td>
  <td style="text-align:center">Pipeline, Hazard, Trap and Interrupt: 2</td>
  <td style="text-align:center">
    <p><a target="_blank" href="https://dl.acm.org/doi/10.1145/1464039.1464045"> "Parallel Operation in the Control Data 6600", Thornton, Proceedings of the Fall Joint Computers Conference, vol 26, pp. 33-40, 1964</a></p>
    <p><a target="_blank" href="http://www.csit-sun.pub.ro/~cpop/Sisteme_cu_Microprocesoare_Avansate_SMPA/SMPA_curs_master5AAC/SMPA_curs2/3_smith.pdf"> "Implementation of Precise Interrupts in Pipelined Processors", Smith, Pleszkun, ISCA, 1985</a></p>
    ddl: 2022.10.26 19:00
  </td>
  <td style="text-align:center"></td>
  </tr>
  <tr>
  <td style="text-align:center">6</td>
  <td style="text-align:center">10/26/2022</td>
  <td style="text-align:center">Memory, SRAM/DRAM, Cache</td>
  <td style="text-align:center">
    <p><a target="_blank" href="https://dl.acm.org/doi/pdf/10.1145/356887.356892"> "Cache Memories", ACM Computing Surveys, Alan J. Smith</a></p>
    <p><a target="_blank" href="https://ieeexplore.ieee.org/document/134547"> "Improving Direct-mapped Cache Performance by the Addition of a Small Fully-Associative Cache and Prefetch buffers". Proceedings of the International Symposium on Computer Architecture (ISCA), 1990, Normal Jouppi</a></p>
    ddl" 2022.11.02 19:00
  </td>
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">7</td>
  <td style="text-align:center">11/02/2022</td>
  <td style="text-align:center">Cache: part 1</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 2.1-2.3</td>
  </tr>

  <tr>
  <td style="text-align:center">8</td>
  <td style="text-align:center">11/09/2022</td>
  <td style="text-align:center">Cache: part 2</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 2.4</td>
  </tr>

  <tr>
  <td style="text-align:center">9</td>
  <td style="text-align:center">11/16/2022</td>
  <td style="text-align:center">Lab Intro</td>
  <td style="text-align:center"></td>
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">10</td>
  <td style="text-align:center">11/26/2022</td>
  <td style="text-align:center">Memory III and Virtual Memory</td>
  <td style="text-align:center"></td>
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">11</td>
  <td style="text-align:center">12/03/2022</td>
  <td style="text-align:center">Pipeline, Out-of-order Issue</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://ieeexplore.ieee.org/document/5392028"> "An Efficient Algorithm for Exploiting Multiple Arithmetic Units"</a></p>
  <p><a target="_blank" href="https://dl.acm.org/doi/10.1145/327070.327125"> "Implementation of precise interrupts in pipelined processors"</a></p>
  ddl： 2022.12.09 23:59
  </td>
  <td style="text-align:center">Chapter 3.1, 3.4, 3.5</td>
  </tr>

  <tr>
  <td style="text-align:center">12</td>
  <td style="text-align:center">12/10/2022</td>
  <td style="text-align:center">Recorder Buffer, Physical Register Management</td>
  <td style="text-align:center">
  </td>
  <td style="text-align:center">Chapter 3.12, 3.13, 3.15</td>
  </tr>

  <tr>
  <td style="text-align:center">13</td>
  <td style="text-align:center">12/23/2022</td>
  <td style="text-align:center">Branch Prediction</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://ieeexplore.ieee.org/document/755465"> "The Alpha 21264 Microprocessor"</a></p>
  <p><a target="_blank" href="https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=491460"> "The MIPS R1000 Superscalar Microprocessor"</a></p>
  ddl： 2022.12.28 17:59
  </td>
  <td style="text-align:center">Chapter 3.3</td>
  </tr>

  <tr>
  <td style="text-align:center">14</td>
  <td style="text-align:center">12/28/2022</td>
  <td style="text-align:center">Branch Prediction and Speculative Execution</td>
  <td style="text-align:center">
  </td>
  <td style="text-align:center">Chapter 3.8, 3.9, Appendix C.2</td>
  </tr>

  <tr>
  <td style="text-align:center">15</td>
  <td style="text-align:center">01/01/2023</td>
  <td style="text-align:center">Load and Store Predicton</td>
  <td style="text-align:center">
  </td>
  <td style="text-align:center"></td>
  </tr>

  <tr>
  <td style="text-align:center">16</td>
  <td style="text-align:center">01/04/2022</td>
  <td style="text-align:center">Instruction Level Parallelism</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://www.hpl.hp.com/techreports/Compaq-DEC/WRL-93-6.pdf"> "David Wall, Limits of Instruction-Level Parallelism"</a></p>
	<p><a target="_blank" href="https://dl.acm.org/doi/10.1145/384286.264201"> "Complexity-effective superscalar processors"</a></p>
	<p><a target="_blank" href="https://minds.wisconsin.edu/handle/1793/60082"> "Quantifying the Complexity of Superscalar Processors"</a></p>
  ddl： 2023.01.07 11:59!
  </td>
  <td style="text-align:center">Chapter 3.12</td>
  </tr>

  <tr>
  <td style="text-align:center">17</td>
  <td style="text-align:center">01/08/2022</td>
  <td style="text-align:center">Introduction to VLIW</td>
  <td style="text-align:center">
 <p><a target="_blank" href="https://dl.acm.org/doi/abs/10.1145/800046.801649"> "Very Long Instruction Word Architectures and the ELI-512"</a></p>
 <p><a target="_blank" href="https://www.google.com/url?sa=t&source=web&rct=j&url=http://inst.eecs.berkeley.edu/~cs252/sp17/papers/TRACE.pdf&ved=2ahUKEwjoycLJmbr8AhVYyHMBHcrzDiQQFnoECBIQAQ&usg=AOvVaw3UCU7YrBdEL95VBQxsaOxO"> "A VLIW Architecture for a Trace Scheduling Compiler"</a></p>
  ddl： 2023.01.11 11:59!
  </td>
  <td style="text-align:center">Appendix H</td>
  </tr>

  <tr>
  <td style="text-align:center">18</td>
  <td style="text-align:center">01/08/2022</td>
  <td style="text-align:center">Introduction to VLIW</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://dl.acm.org/doi/10.1145/225830.224449"> "Simultaneous multithreading: maximizing on-chip parallelism"</a></p>
  <p><a target="_blank" href="https://dl.acm.org/doi/10.1145/359327.359336"> "The CRAY-1 computer system"</a></p>
  ddl： 2023.01.14 23:59!
  </td>
  <td style="text-align:center">Chapter 3.11</td>
  </tr>

  </tbody>
  </table>
</div>