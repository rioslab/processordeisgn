# Lab 2
## Version
1.0
## Due Time
DDL: 12/14/2022, 23:59:59. Git commit time will be checked.
## Grouping
Work independently: 1 student 1 group

## Explanation
In Lab 1, you initially learned the mechanisms of spike and co-sim, got an impression of GreenRio's design, and started exploring the back-end EDA process.
Therefore, Lab 2 will be further improved on the basis of Lab 1.
### CPU Modeling
First, there is CPU modeling. 
Spike is a functional model that can only be used to verify the correctness of your design and not to effectively respond to the performance of your design. Therefore, you need to try to design a performance model.
#### Why a performance model is required
Silicon is immutable, but through the performance analysis of the architecture, you can find out the bottlenecks of the current architecture, and then you can modify RTLs, etc. to optimize the design. In addition, software designers can be instructed to write codes that match hardware resources/execution strategies based on a full understanding of the current hardware architecture to improve efficiency.
#### What is a performance model
You should have a preliminary understanding of it through course learning, paper reading (such as: Asim: A Performance Model Framework), and laboratory projects. In this case, the model is based on C/C++. By simulating performance-related parameters such as time series, the C PU design performance is predicted.
#### Basics of an experimental model
Reference: the C model designed by Lutong Zhang

You need write from scratch
### Co-sim
In Lab2, please complete the co-sim work of your c model and GreenRio like Lab1. Note that here you only need to complete the frontend part of the c model: before the decode (including the decode) in the GreenRio design.
### EDA: floorplan, placement, CTS and global routing
#### Floorplan
A physical design engineer’s main focus is to achieve a decent Quality of Result (QoR) and optimized Power Performance and Area (PPA). The start of this journey begins with the Floorplan steps. What will you achieve at the end of PnR is depends on how good your floorplan is. In case of a macro dominating block, the importance of quality floorplan is quite more. To achieve a good floorplan in a macro dominating block, it might take several iterations and also requires good experience. A detailed analysis of data flow, hierarchy, macro to input-output pins connection, logical depth and many more factors which need to understand and analyzed thoroughly to produce a good floorplan. In this article, we will discuss some of the basic rules on which are helpful to produce a good floorplan and so good QoR.
#### Placement
Placement is the process of placing the standard cells inside the core boundary in an optimal location. The tool tries to place the standard cell in such a way that the design should have minimal congestions and the best timing. Every PnR tool provides various commands/switches so that users can optimize the design in a better way in terms of timing, congestion, area, and power as per their requirements. Based on the preferences set by the user, the tool tray to place and optimize it for better QoR. Placement does not place only the standard cells present in the synthesized netlist but also places many physical only cells and adds buffers/inverters as per the requirement to meet the timings, DRV, and foundry requirements. Here are the basic steps which the tool performs during the placement and optimization stage.
#### CTS
Clock tree synthesis is a process of building and optimizing the clock tree in such a way that the clock gets distributed evenly and every sequential element gets the clock within a target global skew limit. To build the clock tree we have to provide certain constraints as input to the APR tool, which commonly known as clock constraints, and in the case of the Innovus tool, this constraint file is popularly known as ccopt file.
#### Global routing
In the GR (Global Routing) stage, the wiring area will be divided into matrix blocks called GCells. According to these GCells, GR can build a coarse-grained 3D routing graph. In these three-dimensional wiring diagrams, Capabilities and various Constraints can be allocated to edges and vertices, so that the overall routing topology and the metal layer assignment can take into account the routability, clock, crosstalk, energy efficiency, etc. (routability, timing, crosstalk, power, etc).

## Requirement
The projects in this course target the top-down flow to design the CPU.

Concepts and topics of CPU performance modeling, modern microprocessor design and ASIC EDA flow are addressed. 
## 
In Lab 2, you need to improve the performance of the original GreenRio by modify its frontend(e.g. instruction fetch stage, instruction buffer, decode stage) design, alone with pushing forward your eda flow to P&R and analysis timing reports:
  1. You need to improve your CPU design by modifying the basic CPU infrastructure, you can improving its frontend performance by modifying instruction buffer, branch predictor, etc..
  2. You need run the open EDA flow on OpenLANE, in Lab 1, you should finish the RTL synthesis and STA work, in Lab 2, you should push forward through floorplanning, placement, CTS, global routing work.

For lab 2, Synopsys VCS or verilator is the recommended simulator. 

## Grading 
The grading in divided into 4 parts, the proportion is:
        
1. C model for original GreenRio frontend - 25%
   1. C frontend model(original), performance similarity of C model & RTL must within 20%, the less the better - 25%
2. GreenRio pipeline performance improvement: frontend - 25%
   1. (based on 1.a) C frontend model (improved) - 15%
   2. (based on 2.a) RTL implementation, performance similarity of C model & RTL must within 20%, or the C model would be invalid - 10%
3. Open EDA flow - 40%
   1. (based on 2.b) GreenRio core(improved) RTL synthesis - 0%
    2. (based on 3.a) GreenRio core(improved) floorplanning, placement and CTS- 20%
    3. (based on 3.b) GreenRio core(improved) global routing - 20%
4. Lab presentation - 10%
    1. Presentation and Q&A - 10%
         

## Tutorial
### 0. RISC-V ISA and RISC-V Tools
RISC-V (pronounced “risk-five”) is a new instruction set architecture (ISA) designed to support computer architecture research and education. It was developed by the Berkeley Architecture Group (now part of the ASPIRE Lab). RISC-V is a free, open ISA, and it is the fifth RISC instruction set
that has been developed at Berkeley. The base ISA was designed to be clean, simple, and suitable
for direct hardware implementation.
The base instructions of the RISC-V ISA are similar to those of other RISC instruction sets, such
as MIPS or OpenRISC. A summary of some of these 32-bit instructions is shown in the table below.
For more information about the RISC-V ISA, see [www.riscv.org](https://www.riscv.org).

The RISC-V toolchain is a standard GNU cross compiler toolchain ported for RISC-V. You will
use riscv-gcc to compile, assemble, and link your source files. riscv-gcc behaves similarly to
the standard gcc, except that it produces binaries encoded in the RISC-V instruction set. These
compiled binaries can be run on spike, the RISC-V ISA simulator. They can also be used to
generate a hexadecimal list of machine code instructions that can be loaded into the instruction
memory of a simulated (or real) processor.
### 1. GreenRio
GreenRio, AKA hehe,  is a small out-of-order RISC-V core written in synthesizable Verilog that supports the RV64IC unprivileged ISA and parts of the privileged ISA, namely M-mode. You can get the document and source code from [https://github.com/0616ygh/riosclass_template](https://github.com/0616ygh/riosclass_template).

![Screenshot](images/3.png)

### 2. C model
The C model in our laboratory will be a good reference.
### 3. OpenLANE
OpenLANE is an automated RTL to GDSII flow based on several components including OpenROAD, Yosys, Magic, Netgen, Fault, OpenPhySyn, CVC, SPEF-Extractor, CU-GR, Klayout and custom methodology scripts for design exploration and optimization. The flow performs full ASIC implementation steps from RTL all the way down to GDSII - this capability will be released in the coming weeks with completed SoC design examples that have been sent to SkyWater for fabrication. 
For more detailed information, please refer to [OpenLANE Documentation](https://openlane.readthedocs.io/).

#### Run OpenLANE
To run the flow, you can refer to the above documents.

![Screenshot](images/2.png)
### 4. Verilog & SystemVerilog
For beginners, please refer to the Verilog and SystemVerilog tutorials in [ASIC WORLD](https://www.asic-world.com/)

## References
### RTL Design
[Digital Design with RTL Design, VHDL, and Verilog, Frank Vahid](https://www.amazon.com/Digital-Design-RTL-VHDL-Verilog/dp/0470531088)

[Computer Architecture: A Quantitative Approach, J. L. Hennessy and D. A. Patterson](https://www.amazon.com/Computer-Architecture-Quantitative-John-Hennessy/dp/012383872X)

[Verilog Tutorials, Deepak Kumar Tala](http://classweb.ece.umd.edu/enee359a/verilog_tutorial.pdf)
### Synthesis Tools
Yosys[tutorial](https://github.com/YosysHQ/yosys)(open source)

Synopsys DC [tutorial](https://inst.eecs.berkeley.edu/~cs250/fa10/handouts/tut5-dc.pdf)(commercial)

Cadence RTL Compiler / Genus [tutorial](https://community.cadence.com/cadence_blogs_8/b/ld/posts/rtl-compiler-beginner-s-guides-available-on-cadence-online-support)(commercial)

### RTL Simulation Tools
Synopsys VCS [tutorial](https://inst.eecs.berkeley.edu/~cs250/fa10/handouts/tut4-vcs.pdf)(commercial)

Cadence NCSim [tutorial](https://community.cadence.com/cadence_technology_forums/f/functional-verification/35689/tutorial-on-ncsim)(commercial)

Verilator [tutorial](https://www.veripool.org/wiki/verilator)(open source)
### C Model
Functional RISC-V ISA Simulator:[RISC-V Spike](https://github.com/riscv/riscv-isa-sim)

Tutorial on Spike Internal [Tutorial on Spike Internal](https://github.com/poweihuang17/Documentation_Spike)

LowRISC[Running simulations using Spike](https://lowrisc.org/docs/tagged-memory-v0.1/spike/)
### OpenMPW Caravel User Project [Caravel](https://github.com/efabless/caravel_user_project)