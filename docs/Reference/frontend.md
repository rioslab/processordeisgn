# Reference for Frontend

## Coding

Verilog Tutorial: [https://www.chipverify.com/verilog/verilog-tutorial](https://www.chipverify.com/verilog/verilog-tutorial)

Verilog Coding Website:[https://hdlbits.01xz.net/wiki/Problem_sets](https://hdlbits.01xz.net/wiki/Problem_sets)

SystemVerilog Tutorial: [https://www.chipverify.com/systemverilog/systemverilog-tutorial](https://www.chipverify.com/systemverilog/systemverilog-tutorial)

## Books

Here are some books for you.

Computer Architecture, Sixth Edition: A Quantitative Approach:  John L. Hennessy / David A. Patterson

Modern Processor Design: John Paul Shen / Mikko H. Lipasti

## On-line Video

CS61C: [https://www.bilibili.com/video/BV15W4y1S7Lc](https://www.bilibili.com/video/BV15W4y1S7Lc)

CS162: [https://www.bilibili.com/video/BV1ab4y1b7BU](https://www.bilibili.com/video/BV1ab4y1b7BU)

