# Lab 3
## Version
1.0
## Due Time
DDL: 01/15/2023,  11:59:59. Git commit time will be checked.
## Grouping
Work independently: 1 student 1 group
## Requirement
In Lab 3, you need to improve the performance of the original Greenrio by modifying its microarchitecture design, along with pushing forward your eda flow to signoff flow and power analysis, getting the PPA report and GDS.
You need to improve your CPU design referring to your performance model, as there may be some unnecessary stalls in the recent Greenrio pipeline
You should compare the performance log produced by the RTL against your c model, instruction by instruction, to find out what caused the original Greenrio performance to be worse than it should be, and fix it.
You can improve performance by modifying buffer design, ROB module, etc.

In Lab 1 and 2, you should finish the RTL synthesis and place&route work, you may have many issues and bugs to report to the developers of OpenLANE, and we want you to inform your feedback on GitHub, to help the tool improve. Besides, we hope you can finish the flow without DRC/timing errors or warnings.


## Grading 

The grading is divided into 3 parts, the proportion is listed as follows. 

1. GreenRio microarchitecture performance improvement - 50%
    1. Finishing your Greenrio performance model (frontend, backend, and uncore part) - 25%
        0. For some students, please complete lab2 - 0%
        1. You need to complete the backend model - 5%
        2. You need to complete the uncore model: L1 data cache, instruction cache, and memory interface - 15%
        3. Your model needs to be verified by passing some ISA test or benchmarks - 5%
    2. Searching for original Greenrio performance issues/bugs by comparing against the model, the more the better - 10%
        1. Find out the improvement points of the core part: such as fetch, rob, lsu, or buffer design. Please find at least one place that can be improved and explain the reason for your modification. - 5%
        2. Find out the improvement points of the uncore part. For example, change the blocking design to a non-blocking design. Please find at least one place that can be improved and explain the reason for your modification. - 10%
    3. Fixing the performance issues/bugs, implementing your new strategy in RTL, and improving the microarchitecture design.Its performance output should close to your model, diff at most 10%, the less the better - 10%

2. EDA flow - 40%
    1. Greenrio core RTL to GDS flow by openlane- 40%
        1. GDS generation - 10%
        2. Signoff report generation: your flow should pass DRC/LVS check - 15%
            1. In the lab report, briefly describe what DRC is; the DRC problems you have encountered (If you have not encountered such a problem, please skip it), and the common solutions. - 5%
            2. In the lab report, briefly describe what LVS is; the LVS problems you have encountered (If you have not encountered such a problem, please skip it), and the common solutions. - 5%
            3. Briefly analyze your signoff results in the lab report. If a warning/error still exists, point them out; If all passes, give final report information. - 5%
        3. There should be no setup or hold timing violations - 15%
            1. No setup violations - 7.5%
            2. No hold violations -7.5%

3.  Lab report - 10%
    1. Lab report and Presentation- 10% 

##
