# Lab 1
## Version
2.0
## Due Time
DDL: 10/26/2022, 23:59:59. Git commit time will be checked.
## Grouping
Work independently: 1 student 1 group

## Explanation
First, you need a github account.

Secondly, you need to fork this repo: https://github.com/0616ygh/riosclass_template.

Then, all your operations should be carried out in your repo.

Finally, we hope you put the lab report under the path of "rpt"; put the co-sim code in the "co-sim" path; update the modifications to the bugs in the rtl code in the "verilog" path. The work related to the back-end (logic synthesis, floorplan, etc.) is updated in the "openlane" directory.

For Openlane and Yosys, you can 'source env.sh' and then 'make setup' for EDA enviroment. 

See the reference for more information.

## Requirement
The projects in this course target the top-down flow to design the CPU.

Concepts and topics of CPU performance modeling, modern microprocessor design and ASIC EDA flow are addressed. 

In lab 1, you will know how to model a CPU, understand logic synthesis, perform simulation, and finish the co-sim execution of GreeRio RTL + Spike Model. Besides, you need to learn microarchitecture and practice your verilog programming ability, which will be crucial to your later projects.

For lab 1, Synopsys VCS is the recommended simulator. 

## Grading
The grading is divided into 3 parts, the proportion is:

1. Spike model and co-sim - 50%
    1. Spike model (with risc-v pk) execution correctness: simulate programs atop the proxy kernel  - 10%
    2. GreenRio core RTL execution correctness: pass some ISA tests (Synopsys VCS environment)- 10%
    3. Spike model + GreenRio RTL co-simulation system - 30%
        1. You need to run the elf file on the spike emulator and print the log - 5%
        2. You need to have GreenRio run the risc-v elf in an RTL simulation environment and get the corresponding results: register states, values, etc. -5%
        3. Spike + RTL co-sim: transaction-level co-simulation framework should be finished -20%
            1. Front-end co-sim: use the same ISA/program/elf to compare the results in the decode phase (spike and RTL) - 10%
            2. Back-end co-sim: compare commit order, register states, memory states, register values, pc values, etc. (spike and RTL) - 10%

2. Open EDA flow - 40%
    1. GreenRio core logic synthesis by yosys (use 2 libs as different conrer) - 30%
    2. GreenRio core gate level netlist + Spike co-sim execution correctness 10%

3. Presentation and Q&A - 10%
    1. Pre and Q&A - 10%

## 

## Spike and Co-sim
In lab1, you need install spike and make it work correct. 

Besides, you should clone lab repo from [here](https://github.com/0616ygh/riosclass_template.git), and configure it in your environment so that the RTL codes can run basic tests: ISA test and [RISC-V Torture](https://github.com/ucb-bar/riscv-torture) in VCS simulation environment.

Finally, you need to use spike to build a co-sim framwork for RTL code debugging and testing. 

    1. Read the spike documentation, understand the spike working mechanism, install and run spike with pk normally;
    2. Get and read the source code of GreenRio, understand its architecture, complete its verification and test environment (Synopsys VCS), and be able to run ISA test and RISC-V torture;
    3. Use Spike to complete the design of GreenRio's co-sim framework, enabling GreenRio and Spike to work together. You need to compare the results in the decode phase (spike and RTL) as front-end co-sim, and compare commit order, register states, memory states, register values, pc values, etc. of spike and RTL in the commit state as back-end co-sim. Besides, you can pass the value obtained by the front-end of the spike to the back-end of the rtl, compare the output of different backends (register values and status, PC values, etc.). You can design this framework in any language, such as c++/c, python, etc. If you find a bug in RTL in the process, we're glad that you can write it into your lab report, which will give you extra credit.


## EDA and OpenLANE
You should use GreenRio core as a baseline code, use open source synthesis tool [yosys](https://github.com/YosysHQ/yosys) (it's part of OpenLANE flow) to get the gate level netlist.

    1. For Lab1, you only need to finish the logic synthesis part and get the gate level netlist (typically a .v file).
    2. As a verilog file, the netlist should perform the same behavior as the register transfer level source file. So after getting the logic synthesis output, you need to replace the RTL part in the "RTL + Spike" with the gate level netlist, then run through the same test suite to make sure it performs exactly the same behavior as RTL source file.
    3. Notice, after you perform logic synthesis, you should do logic equivalence checking (LEC) for looking at the combinatorial structure of the design to determine if the structure of two alternative implementations will exhibit the same behavior. Cadence Conformal can be used for LEC and you can get it on cad 4 or 5.
    4. For netlist co-sim, you may need a perfect memory model.

## Tutorial
### 0. RISC-V ISA and RISC-V Tools
RISC-V (pronounced “risk-five”) is a new instruction set architecture (ISA) designed to support computer architecture research and education. It was developed by the Berkeley Architecture Group (now part of the ASPIRE Lab). RISC-V is a free, open ISA, and it is the fifth RISC instruction set
that has been developed at Berkeley. The base ISA was designed to be clean, simple, and suitable
for direct hardware implementation.
The base instructions of the RISC-V ISA are similar to those of other RISC instruction sets, such
as MIPS or OpenRISC. A summary of some of these 32-bit instructions is shown in the table below.
For more information about the RISC-V ISA, see [www.riscv.org](https://www.riscv.org).

The RISC-V toolchain is a standard GNU cross compiler toolchain ported for RISC-V. You will
use riscv-gcc to compile, assemble, and link your source files. riscv-gcc behaves similarly to
the standard gcc, except that it produces binaries encoded in the RISC-V instruction set. These
compiled binaries can be run on spike, the RISC-V ISA simulator. They can also be used to
generate a hexadecimal list of machine code instructions that can be loaded into the instruction
memory of a simulated (or real) processor.
### 1. GreenRio
GreenRio, AKA hehe,  is a small out-of-order RISC-V core written in synthesizable Verilog that supports the RV64IC unprivileged ISA and parts of the privileged ISA, namely M-mode. You can get the document and source code from [https://github.com/0616ygh/riosclass_template](https://github.com/0616ygh/riosclass_template).

![Screenshot](images/3.png)

### 2. Spike
Spike, the RISC-V ISA Simulator, implements a functional model of one or more RISC-V harts. It is named after the golden spike used to celebrate the completion of the US transcontinental railway.

Functionally, Spike (riscv-isa-sim) is an instruction level emulator developed based on C/C++, which provides a development and test execution environment for RISC-V architecture. Spike implements a general instruction execution environment through software, including memory system, virtual and physical address space, processor pipeline supporting all ISA extensions of RISC-V, and the most basic I/O interface and disk system (with
riscv pk). At the same time, Spike supports many debugging mechanisms, such as using the GDB debugger to debug Spike itself. In addition, with OpenOCD, you can directly debug RISC-V programs running on Spike with GDB.

Case study: [Ibex core verification](https://github.com/lowRISC/ibex/tree/master/dv/uvm)

![Screenshot](images/1.png)

For moer infomation, please refer to [spike githuab](https://github.com/riscv-software-src/riscv-isa-sim).

For co-sim, sail-rtl co-sim framwork is a refrence for you. 
### 3. OpenLANE
OpenLANE is an automated RTL to GDSII flow based on several components including OpenROAD, Yosys, Magic, Netgen, Fault, OpenPhySyn, CVC, SPEF-Extractor, CU-GR, Klayout and custom methodology scripts for design exploration and optimization. The flow performs full ASIC implementation steps from RTL all the way down to GDSII - this capability will be released in the coming weeks with completed SoC design examples that have been sent to SkyWater for fabrication. 
For more detailed information, please refer to [OpenLANE Documentation](https://openlane.readthedocs.io/).

#### Run OpenLANE
To run the flow, you can refer to the above documents.

![Screenshot](images/2.png)
### 4. Verilog & SystemVerilog
For beginners, please refer to the Verilog and SystemVerilog tutorials in [ASIC WORLD](https://www.asic-world.com/)

## References
### RTL Design
[Digital Design with RTL Design, VHDL, and Verilog, Frank Vahid](https://www.amazon.com/Digital-Design-RTL-VHDL-Verilog/dp/0470531088)

[Computer Architecture: A Quantitative Approach, J. L. Hennessy and D. A. Patterson](https://www.amazon.com/Computer-Architecture-Quantitative-John-Hennessy/dp/012383872X)

[Verilog Tutorials, Deepak Kumar Tala](http://classweb.ece.umd.edu/enee359a/verilog_tutorial.pdf)
### Synthesis Tools
Yosys[tutorial](https://github.com/YosysHQ/yosys)(open source)

Synopsys DC [tutorial](https://inst.eecs.berkeley.edu/~cs250/fa10/handouts/tut5-dc.pdf)(commercial)

Cadence RTL Compiler / Genus [tutorial](https://community.cadence.com/cadence_blogs_8/b/ld/posts/rtl-compiler-beginner-s-guides-available-on-cadence-online-support)(commercial)

### RTL Simulation Tools
Synopsys VCS [tutorial](https://inst.eecs.berkeley.edu/~cs250/fa10/handouts/tut4-vcs.pdf)(commercial)

Cadence NCSim [tutorial](https://community.cadence.com/cadence_technology_forums/f/functional-verification/35689/tutorial-on-ncsim)(commercial)

Verilator [tutorial](https://www.veripool.org/wiki/verilator)(open source)
### C Model
Functional RISC-V ISA Simulator:[RISC-V Spike](https://github.com/riscv/riscv-isa-sim)

Tutorial on Spike Internal [Tutorial on Spike Internal](https://github.com/poweihuang17/Documentation_Spike)

LowRISC[Running simulations using Spike](https://lowrisc.org/docs/tagged-memory-v0.1/spike/)
### OpenMPW Caravel User Project [Caravel](https://github.com/efabless/caravel_user_project)